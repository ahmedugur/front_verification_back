import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {HomeModule} from './ui-panel/home/home.module';
import {PageNotFoundModule} from './ui-panel/page-not-found/page-not-found.module';
import {SubmitRequestModule} from './ui-panel/home/pages/submit-request/submit-request.module';
import {NzNotificationModule} from 'ng-zorro-antd';
import {RecaptchaModule} from 'ng-recaptcha';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    RouterModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RecaptchaModule,
    BrowserAnimationsModule,
    HomeModule,
    SubmitRequestModule,
    NzNotificationModule,
    PageNotFoundModule,
    AppRoutingModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
