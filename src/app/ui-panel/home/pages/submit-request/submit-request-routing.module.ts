import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SubmitRequestComponent} from './submit-request.component';

const routes: Routes = [
  {
    path: '',
    component: SubmitRequestComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubmitRequestRoutingModule {
}
