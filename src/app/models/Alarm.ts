export class Alarm {
  public callerSurname: string;
  public callerName: string;
  public callerMidName: any;
  public callerPhone: any;
  public callerEmail: any;
  public address: any;
  public placeTypeId: any;
  public description: any;
  public createDate: any;
  public status: any;
  public resultCallerText: any;
  public publicCode: any;
  public requestChannelId: any;
  public file: any;
  public events: any;
  public history: any;
}
