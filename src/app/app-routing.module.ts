import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PageNotFoundComponent} from './ui-panel/page-not-found/page-not-found.component';

const routes: Routes = [

  {
    path: '',
    loadChildren: './ui-panel/home/home.module#HomeModule',
  },
  {
    path: '**', component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
